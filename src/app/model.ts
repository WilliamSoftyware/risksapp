export enum Impact {
    MINEUR = "Mineur",
    MAJEUR = "Majeur",
    GRAVE = "Grave",
    CATASTROPHIQUE = "Catastrophique",

}

export enum Probabilite {
    IMPROBABLE = "Improbable",
    PEU_PROBABLE = "Peu probable",
    PROBABLE = "Probable",
    TRES_PROBABLE = "Très probable",
}

export enum Criticite {
    PEU_CRITIQUE = "Peu critique",
    CRITIQUE = "Critique",
    TRES_CRITIQUE = "Très critique",
}

export interface Risk{
    id: number;
    impact: Impact;
    probabilite: Probabilite;
    criticite : Criticite;
    nature_risk: string;
    description: string;
}

export interface Frequence {
    classe: string;
    intitule : string;
    quantitatives : string;
    qualitatives : string;
}

export interface Gravite {
    niveau: string;
    description: string;
}