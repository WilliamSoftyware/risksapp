import { Component, OnInit } from '@angular/core';
import { Frequence } from '../model';
@Component({
  selector: 'app-tab-proba',
  templateUrl: './tab-proba.component.html',
  styleUrls: ['./tab-proba.component.scss'],
})
export class TabProbaComponent {
  constructor() {}
  frequences: Frequence[] = [
    {
      classe: 'v1(F)',
      intitule: 'Impossibile à improbable',
      quantitatives: '< 1 fois en 5 ans',
      qualitatives: 'Jamais vu',
    },
    {
      classe: 'v2(F)',
      intitule: 'Peu probable',
      quantitatives: ' 1 fois par mois',
      qualitatives: "Vu dans d'autres organisations",
    },
    {
      classe: 'v3(F)',
      intitule: 'Probable',
      quantitatives: '< 1 fois par semaine',
      qualitatives: "survient dans l'organisation",
    },
    {
      classe: 'v4(F)',
      intitule: 'Itrès probable à certain',
      quantitatives: ' 1 fois par jour',
      qualitatives: "Vécu dans mon secteur d'activité",
    },
  ];
}
