import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabProbaComponent } from './tab-proba.component';

describe('TabProbaComponent', () => {
  let component: TabProbaComponent;
  let fixture: ComponentFixture<TabProbaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TabProbaComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabProbaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
