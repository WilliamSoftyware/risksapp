import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Criticite, Impact, Probabilite } from '../model';

@Component({
  selector: 'app-form-risk',
  templateUrl: './form-risk.component.html',
  styleUrls: ['./form-risk.component.scss']
})
export class FormRiskComponent implements OnInit {

  impacts = Object.values(Impact);
  probabilites = Object.values(Probabilite);
  criticites = Object.values(Criticite);

  riskForm = new FormGroup({
    impact: new FormControl(''),
    probabilite: new FormControl(''),
    criticite: new FormControl(''),
    nature_risk: new FormControl(''),
    description: new FormControl(''),
  });

  constructor() { }

  ngOnInit(): void {
  }

  addRisk(){
    console.log(this.riskForm.value)
  }

}
