import { Component, OnInit } from '@angular/core';
import { Gravite } from '../model';

@Component({
  selector: 'app-tableau-gravite-impact',
  templateUrl: './tableau-gravite-impact.component.html',
  styleUrls: ['./tableau-gravite-impact.component.scss'],
})
export class TableauGraviteImpactComponent {
  constructor() {}

  gravites: Gravite[] = [
    {
      niveau: 'Niveau 1',
      description:
        'Gravité mineure : conséquences mineures sans impact / préjudices pour le projet (ex: retard simple).',
    },
    {
      niveau: 'Niveau 2',
      description:
        "Gravité majeure : incident avec impact pour le projet (report, prolongation anormales des délais, non satisfaction d'une exigence)",
    },
    {
      niveau: 'Niveau 3',
      description:
        "Gravité critique : conséquences graves pour le projet (risque liée à la santé et sécurité des utilisateurs, non respect d'une exigence légale et règlementaire, non satisfaction d'un objectif).",
    },
    {
      niveau: 'Niveau 4',
      description:
        "Gravité catastrophique : conséquences très graves pour le projet (non satisfaction des objectifs du projet, compromission des réglements, atteinte à l'image de l'organisation, perte financière).",
    },
  ];
}
