import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauGraviteImpactComponent } from './tableau-gravite-impact.component';

describe('TableauGraviteImpactComponent', () => {
  let component: TableauGraviteImpactComponent;
  let fixture: ComponentFixture<TableauGraviteImpactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableauGraviteImpactComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauGraviteImpactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
